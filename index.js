//Mathematical Operations (-, *, /, %)
//substraction
let numString1 = "5";
let numString2 = "6";
let num1 = 5;
let num2 = 6;
let num3 = 5.5;
let num4 = .5;

console.log(num1-num3); //-0.5 results in proper mathematical operation
console.log(num3-num4); //5 results in proper mathematical operation
console.log(numString1-num2); // -1 results in proper mathematical operation because the string was forced to become a number
console.log(numString2-num2); //0
console.log(numString1-numString2); //-1 in substraction, numeric strings will not concatenate and instead will beforcibly changed its type and substract properly

let sample2 = "Juan Dela Cruz";
console.log(sample2-numString1); //NaN - results is not a number. when trying to perform substraction between alphanumeric string and numeric string, the result is NaN.

//Multiplication
console.log(num1*num2); //30
console.log(numString1*num1); //25
console.log(numString1*numString2);//30
console.log(sample2*5); //NaN

let product1 = num1 * num2;
let product2 = numString1 * num1;
let product3 = numString1 * numString2;
console.log(product1);

// Division
console.log(product1/num2); //5
console.log(product2/5);
console.log(numString2/numString1); //1.2
console.log(numString2%numString1); //1

// division/multiplication by 0
console.log(product2*0); //0
console.log(product3/0); //0
// Division by 0 is not accurately and should not be done, it results to infinity


// % Modulo - remainder of division operation
console.log(product2%num2); //product 2/num2 (25/6) = remainder - 1
console.log(product3%product2); //30/25 = remainder - 5
console.log(num1%num2); //5
console.log(num1%num1); //0

// Boolean (true or false)
/*
	Boolean is usually used for logic operations or if-else condition
		- boolean values are normally used to store values relating to the state of certain things
	When creating a variable which will contain a boolean, the variable name is usually a yes or no question
*/

	let isAdmin = true;
	let isMarried = false;
	let isMVP = true;

	// You can also concarenate strings + boolean
	console.log("Is she married? " + isMarried);
	console.log("Is he the MVP? " + isMVP);
	console.log(`Is he the current Admind? ` + isAdmin);

// Arrays
/*
	Arrays 
		- are a special kind of data type to store multiple values
		- can actually store data with different types BUT as the best practice, arrays are used to contain multiple values of the SAME data type
		- values in arrays are separated by commas; an arrayiscreated  with  an array literal = []
*/
//syntax
	//let/const arrayName = [elementA, elementB, elementC, ...]
let array1 = ["Serin", "Nazir", "Aurelius", "Alexis"];
console.log(array1);
let array2 = ["Royal Alchemist", true, 500, "Otome"];
console.log(array2); // not recommended

let grades = [98.7, 92.1, 90.2, 94.6];
console.log(grades);

// Array are better thought as group data

//Objcts
/*
	Objects
		- are another special kind of data type used to mimic the real world
		- used to create complex data that contains pieces  of information that are relevant to each other
		- objects are created with object literals = {}
			- each data/value are paired with a key
			- each field is called a property
			- each field is  separated by comma

		syntax:
			let/const objectName = {
				propertyA: value,
				propertyB: value
			}
*/
let person = {
	fullName: 'Aurelius of Eskia',
	age: 19,
	isMarried: true,
	magicAffinity: ["Water, Darkness"],
	eskia: {
		status: 'Youngest Prince of Eskia',
		focus: 'Combat & Etiquette',
		favoriteOST: 'Under the Rain'
	}
};

console.log(person)

/* Mini-Activity
	Create a variable with a group  of data
		- The group of data should contain names from your favorite band/idol

	Create a variable which contains multiple values of differing types and describe a single person.
		-This data type should be able  to contain multiple key value pairs:
		firstName: <value>
		lastName: <value>
		isDeveloper: <value>
		hasPortfolio: <value>
		age: <value>
		contact: <value>
		address:
			houseNumber: <value>
			street: <value>
			city: <value>
*/

let bangtanSonyeondan = ['Kim Namjoon', 'Kim Seokjin', 'Min Yoongi', 'Jung Hoseok', 'Park Jimin', 'Kim Taehyung', 'Jeon Jungkook']
console.log(bangtanSonyeondan)

let minYoongi = {
	firstName: 'Yoongi',
	lastName: 'Min',
	isDeveloper: ["Rapper", "Songwriter", "Record Producer"],
	isPortfolio: ["Wine", "Eternal Sunshine", "We Don't Talk Together", "Eight", "You", "That That"],
	age: '29',
	contact: 'Breach of Privacy. No.',
	address: {
		houseNumber: 'Unknown',
		street: 'Buk District',
		city: 'Daegu'
	}
};
console.log(minYoongi)

//Undefined vs Null
	// Null - is explicit absense of data/value. This is done to project that a variable contains nothing over undefined as undefined merely means there is no data in the variable BECAUSE the variable has not been assigned an initial value.

	let sampleNull = null;

	//Undefined - is a representation that a variable has been declared but it was not assigned an initial values

	let sampleUndefined;

	console.log(sampleNull);
	console.log(sampleUndefined);

	// certain processes in programming explicitly return null to indicate that the task resulted to nothing

	let foundResult = null;

	// Example:
	let myNumber = 0;
	let myString = "";
	// using null to compare to a 0 value and an empty string is much better for readability


	// for undefined, this is normally caused by developers creating variables that have no value  or data/associated with them.
	// this is when a variable does exist but its value is still unknown
	let person2 = {
		name: "Daryll",
		age: 29
	}
	//because person2 does exist but the property isAdmin does not.
	console.log(person2.isAdmin);

/* [SECTION] Functions
	Functions
		- in JavaScript are line/block of codesthat tell our device/application to perform a certain task when called/invoked
		- are reusable pieces of code with instructions which used OAOA just as long as we can call/invoke them

		Syntax:
			function functionName(){
				code block
					- block of code that will be executed oncethe  function has been run/called/invoked	
			}

*/

// Declaring function
function printName1(){
	console.log("Aurelius of Eskia");
};

//invoking/calling function - functionName()
printName();
printName();
printName();

function showSum(){
	console.log(19+20);
};

showSum();

// Note: do not create functions with the same name

/* Parameters and Arguements
	"name" is called parameter
		a parameter acts as a named variable/container that exists ONLY inside the function. This isused to store information/ to act as a stand-in or simply the contain the value passed into  thefunction as an arguement.
*/

function printName(prince){
	console.log(`${prince}, Youngest Prince of Eskia`);
};

// When a function is invoked and a data is passed, we call the data as arguement
// In this invocation, "Aurelius" is an arguement passed into our printName function and is represented by the "prince" parameter within our function.
// data passed into the function: arguement
// representation of the arguement within the function: parameter
printName("Aurelius");

function displayNum(number){
	alert(number);
};

displayNum(9000);

/* Mini-Activity:
	Create a function which will be able to show message in the console.
		The message should be able to be passed into function via an arguement.

			- Sample Message:
				"JavaScript is Fun"
				"C++ is Fun"

				arguement: programming language

*/

function progLang(programmingLanguage){
	console.log(`${programmingLanguage} is Fun`);
};

progLang("Python");
progLang("C++");

// Multiple parameters and arguements
function displayFullName(firstName, mi, lastName, age){
	console.log(`${firstName} ${mi} ${lastName} ${age}`);
}

displayFullName("Vil", "E", "Schoenheit", 18);

// return keyword
	// The "return" statement allows the output of a function to be passed to the the line/block of code that invoked/called the function
	// any line/block that comes after the return statement is ignored because it ends the function execution

	function createFullName(firstName, middleName, lastName){
		// return is used so that a function may return a value
		// it also stops the process of the function any other instruction after the keyword will not be  processed
		return `${firstName} ${middleName} ${lastName}`
		console.log("I will no longer run because the function's value/result has been returned.")
	};

	let fullName1 = createFullName("Min", "SUGA", "Yoongi");
	let fullName2 = displayFullName("Kim", "RM", "Namjoon");
	let fullName3 = createFullName("Jung", "J-HOPE", "Hoseok")

	console.log(fullName1);
	console.log(fullName2);
	console.log(fullName3);